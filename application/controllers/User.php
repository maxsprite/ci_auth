<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends MY_Controller {

    public function login()
    {
        if ($this->input->get('driver') != NULL)
        {
            $this->load->library('oauth');
            $this->oauth->login($this->input->get('driver'));   
        }
        else
        {
            if (strtolower( $_SERVER['REQUEST_METHOD'] ) == 'post')
            {
                $this->require_min_level(1);   
            }
    
            $this->setup_login_form();
            
            $this->load->view('layout/header');
    		$this->load->view('user/login');
    		$this->load->view('layout/footer');
        }
    }
    
    public function registration()
    {
        if( strtolower( $_SERVER['REQUEST_METHOD'] ) == 'post' )
        {
            // Customize this array for your user
            $user_data = [
                'username'   => $this->input->post('username'),
                'passwd'     => $this->input->post('password'),
                'email'      => $this->input->post('email'),
                'auth_level' => '9',
            ];
    
            $this->is_logged_in();
    
            echo $this->load->view('layout/header', '', TRUE);
    
            // Load resources
            $this->load->model('examples/examples_model');
            $this->load->model('examples/validation_callables');
            $this->load->library('form_validation');
    
            $this->form_validation->set_data( $user_data );
    
            $validation_rules = [
    			[
    				'field' => 'username',
    				'label' => 'username',
    				'rules' => 'max_length[12]|is_unique[' . config_item('user_table') . '.username]',
                    'errors' => [
                        'is_unique' => 'Username already in use.'
                    ]
    			],
    			[
    				'field' => 'passwd',
    				'label' => 'passwd',
    				'rules' => [
                        'trim',
                        'required',
                        [ 
                            '_check_password_strength', 
                            [ $this->validation_callables, '_check_password_strength' ] 
                        ]
                    ],
                    'errors' => [
                        'required' => 'The password field is required.'
                    ]
    			],
    			[
                    'field'  => 'email',
                    'label'  => 'email',
                    'rules'  => 'trim|required|valid_email|is_unique[' . config_item('user_table') . '.email]',
                    'errors' => [
                        'is_unique' => 'Email address already in use.'
                    ]
    			],
    			[
    				'field' => 'auth_level',
    				'label' => 'auth_level',
    				'rules' => 'required|integer|in_list[1,6,9]'
    			]
    		];
    
    		$this->form_validation->set_rules( $validation_rules );
    
    		if( $this->form_validation->run() )
    		{
                $user_data['passwd']     = $this->authentication->hash_passwd($user_data['passwd']);
                $user_data['user_id']    = $this->examples_model->get_unused_id();
                $user_data['created_at'] = date('Y-m-d H:i:s');
    
                // If username is not used, it must be entered into the record as NULL
                if( empty( $user_data['username'] ) )
                {
                    $user_data['username'] = NULL;
                }
    
    			$this->db->set($user_data)
    				->insert(config_item('user_table'));
    
    			if( $this->db->affected_rows() == 1 )
    				echo '<h1>Congratulations</h1>' . '<p>User ' . $user_data['email'] . ' was created.</p>';
    		}
    		else
    		{
    			echo '<h1>User Creation Error(s)</h1>' . validation_errors();
    		}
    
            echo $this->load->view('layout/footer', '', TRUE);
        }
        else 
        {
            $this->load->view('layout/header');
    		$this->load->view('user/registration');
    		$this->load->view('layout/footer');
        }
    }
    
    public function profile()
    {
        if (isset($this->auth_user_id))
        {
            $query = $this->db->query(
                "SELECT user_id, email, username, first_name, last_name FROM users WHERE user_id = $this->auth_user_id LIMIT 1"
            );
            $user = $query->row();
            
            $grav_url = "https://www.gravatar.com/avatar/" . md5( strtolower( trim( $user->email ) ) ) . "?d=" . urlencode( 'http://www.drastic.tv/images/general/linux/tux-48x48.png' ) . "&s=48x48";
            
            $this->load->view('layout/header');
    		$this->load->view('user/profile', [
    		    'user' => $user, 
    		    'avatar_url' => $grav_url
    		]);
    		$this->load->view('layout/footer');   
        }
        else 
        {
            show_404();    
        }
    }
    
    public function oauth_google_callback()
    {
        $this->oauth_callback('google');
    }
    
    public function oauth_linkedin_callback()
    {
        $this->oauth_callback('linkedin');
    }
    
    private function oauth_callback($driver)
    {
        echo $this->load->view('layout/header', '', TRUE);
        
        $this->load->library('oauth');
        $data = $this->oauth->login($driver); 
        
        if (is_object($data)) 
        {
            // Email address retrieved from external authentication
            $email_address = $data->getEmail();
            $query = $this->db->query("SELECT email FROM users WHERE email='$email_address' LIMIT 1");
            $row = $query->row_array();
            
            if( ! empty( $email_address ) && $row['email'] != NULL )
            {
                $this->force_auth($email_address);
            }
            else
            {
                $password = $this->generateStrongPassword();
                // Customize this array for your user
                $user_data = [
                    'username'   => explode('@', $email_address)[0],
                    'passwd'     => $password,
                    'email'      => $email_address,
                    'first_name' => $data->getFirstName(),
                    'last_name'  => $data->getLastName(),
                    'auth_level' => '9', // 9 if you want to login @ examples/index.
                ];
        
                $this->is_logged_in();
        
                // Load resources
                $this->load->model('examples/examples_model');
                $this->load->model('examples/validation_callables');
                $this->load->library('form_validation');
        
                $this->form_validation->set_data( $user_data );
        
                $validation_rules = [
        			[
        				'field' => 'username',
        				'label' => 'username',
        				'rules' => 'max_length[25]|is_unique[' . config_item('user_table') . '.username]',
                        'errors' => [
                            'is_unique' => 'Username already in use.'
                        ]
        			],
        			[
        				'field' => 'passwd',
        				'label' => 'passwd',
        				'rules' => [
                            'trim',
                            'required',
                            [ 
                                '_check_password_strength', 
                                [ $this->validation_callables, '_check_password_strength' ] 
                            ]
                        ],
                        'errors' => [
                            'required' => 'The password field is required.'
                        ]
        			],
        			[
                        'field'  => 'email',
                        'label'  => 'email',
                        'rules'  => 'trim|required|valid_email|is_unique[' . config_item('user_table') . '.email]',
                        'errors' => [
                            'is_unique' => 'Email address already in use.'
                        ]
        			],
        			[
        				'field' => 'auth_level',
        				'label' => 'auth_level',
        				'rules' => 'required|integer|in_list[1,6,9]'
        			]
        		];
        
        		$this->form_validation->set_rules( $validation_rules );
        
        		if( $this->form_validation->run() )
        		{
                    $user_data['passwd']     = $this->authentication->hash_passwd($user_data['passwd']);
                    $user_data['user_id']    = $this->examples_model->get_unused_id();
                    $user_data['created_at'] = date('Y-m-d H:i:s');
        
                    // If username is not used, it must be entered into the record as NULL
                    if( empty( $user_data['username'] ) )
                    {
                        $user_data['username'] = NULL;
                    }
        
        			$this->db->set($user_data)
        				->insert(config_item('user_table'));
        
        			if( $this->db->affected_rows() == 1 )
        				echo '<h1>Congratulations</h1>' . '<p>User ' . $user_data['email'] . ' was created. Your password is ' . $password . '</p>';
                        echo $this->load->view('redirect_script', ['driver' => $driver], TRUE);
        		}
        		else
        		{
        			echo '<h1>User Creation Error(s)</h1>' . validation_errors();
        		}
            }
        }
        else
        {
            throw new Exception('Error in oauth2 function');
        }
        echo $this->load->view('layout/footer', '', TRUE);
    }
    
    private function force_auth($email_address) 
    {
        $auth_model = $this->authentication->auth_model;  
        // Get normal authentication data using email address
        if ($auth_data = $this->{$auth_model}->get_auth_data($email_address))
        {
            /**
             * If redirect param exists, user redirected there.
             * This is entirely optional, and can be removed if 
             * no redirect is desired.
             */
            $this->authentication->redirect_after_login();  
            // Set auth related session / cookies
            $this->authentication->maintain_state( $auth_data );
        }
    }
    
    private function generateStrongPassword($length = 9, $add_dashes = false, $available_sets = 'luds')
    {
    	$sets = array();
    	if(strpos($available_sets, 'l') !== false)
    		$sets[] = 'abcdefghjkmnpqrstuvwxyz';
    	if(strpos($available_sets, 'u') !== false)
    		$sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
    	if(strpos($available_sets, 'd') !== false)
    		$sets[] = '23456789';
    	if(strpos($available_sets, 's') !== false)
    		$sets[] = '!@#$%&*?';
    	$all = '';
    	$password = '';
    	foreach($sets as $set)
    	{
    		$password .= $set[array_rand(str_split($set))];
    		$all .= $set;
    	}
    	$all = str_split($all);
    	for($i = 0; $i < $length - count($sets); $i++)
    		$password .= $all[array_rand($all)];
    	$password = str_shuffle($password);
    	if(!$add_dashes)
    		return $password;
    	$dash_len = floor(sqrt($length));
    	$dash_str = '';
    	while(strlen($password) > $dash_len)
    	{
    		$dash_str .= substr($password, 0, $dash_len) . '-';
    		$password = substr($password, $dash_len);
    	}
    	$dash_str .= $password;
    	return $dash_str;
    }
    
    public function logout()
    {
        $this->authentication->logout();

        // Set redirect protocol
        $redirect_protocol = USE_SSL ? 'https' : NULL;

        redirect( site_url( '/' . '?logout=1', $redirect_protocol ) );
    }
}