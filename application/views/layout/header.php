<!DOCTYPE html>
<html>
    <head>
        <title>Test work</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="/public/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="/public/css/font-awesome.min.css">
        <link rel="stylesheet" href="/public/css/site.css">
    </head>
    <body>
        <nav class="navbar navbar-default">
          <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="<?= site_url('/') ?>">CI Test</a>
            </div>
        
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav">
                <li><a href="<?= site_url('/') ?>">Home</a></li>
                <li><a href="<?= site_url('about') ?>">About</a></li>
              </ul>

              <ul class="nav navbar-nav navbar-right">
                <?php if( ! isset($auth_user_id)): ?>
                  <li><a href="<?= site_url('user/login') ?>">Login</a></li>
                  <li><a href="<?= site_url('user/registration') ?>">Registration</a></li>
                <?php else: ?>
                  <li><a href="<?= site_url('user/profile') ?>">Pofile</a></li>
                  <li><a href="<?= site_url('user/logout') ?>">Logout</a></li>
                <?php endif; ?>
              </ul>
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>
        
        <!-- BEGIN CONTENT -->
        <div class="container">