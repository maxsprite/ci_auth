<div class="col-md-12">
    <h1>User profile.</h1>
    
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><?= $auth_email ?></h3>
      </div>
      <div class="panel-body">
        <div class="text-left">
            <img src="<?= $avatar_url ?>" class="img-thumbnail">
        </div>
        <br>
        <table class="table">
          <tr>
            <td>User ID</td>
            <td><?= $user->user_id ?></td>
          </tr>
          <tr>
            <td>Username</td>
            <td><?= $user->username ?></td>
          </tr>
          <?php if ($user->first_name !== NULL): ?>
            <tr>
              <td>First Name</td>
              <td><?= $user->first_name ?></td>
            </tr>
          <?php endif; ?>
          <?php if ($user->last_name !== NULL): ?>
            <tr>
              <td>Last Name</td>
              <td><?= $user->last_name ?></td>
            </tr>
          <?php endif; ?>
        </table>
      </div>
    </div>
</div>