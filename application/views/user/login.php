<div class="col-md-12">
    <h1>Login page.</h1>
    
    <?php
      	if( isset( $login_error_mesg ) )
      	{
      		echo '
      			<div class="alert alert-danger" role="alert">
      				<p>
      					Login Error #' . $this->authentication->login_errors_count . '/' . config_item('max_allowed_attempts') . ': Invalid Username, Email Address, or Password.
      				</p>
      				<p>
      					Username, email address and password are all case sensitive.
      				</p>
      			</div>
      		';
      	}
    ?>
    
    <?php echo form_open(site_url('login?redirect=/'), ['class' => 'std-form'] );  ?>
      <div class="form-group">
        <label for="exampleInputEmail1">Email address or username</label>
        <input type="text" name="login_string" id="login_string" class="form_input form-control" autocomplete="off" maxlength="255" placeholder="Email or username" />
      </div>
      <div class="form-group">
        <label for="exampleInputPassword1">Password</label>
        
        <input type="password" name="login_pass" id="login_pass" class="form_input password form-control" <?php 
    			if( config_item('max_chars_for_password') > 0 )
    				echo 'maxlength="' . config_item('max_chars_for_password') . '"'; 
    		?> autocomplete="off" readonly="readonly" onfocus="this.removeAttribute('readonly');" placeholder="Password" />

      </div>
      <button type="submit" class="btn btn-default">Submit</button>
    </form>
    <hr/>
    <p class="text-center"><b>OR LOGIN VIA</b></p>
    <div id="login-socials" class="col-md-12 text-center">
      <a href="<?= site_url('user/login?driver=google') ?>">
        <i class="fa fa-google-plus-square" aria-hidden="true"></i>
      </a>
      <a href="<?= site_url('user/login?driver=linkedin') ?>">
        <i class="fa fa-linkedin-square" aria-hidden="true"></i>
      </a>
    </div>
    <div class="clearfix"></div>
    <hr/>
</div>
