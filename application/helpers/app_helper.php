<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class App {
    
    private static $ci = NULL;
    
    public static function ci()
    {
        if (self::$ci == NULL) 
        {
            self::$ci =& get_instance();
        }
        return self::$ci;
    }
}