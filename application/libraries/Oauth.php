<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Oauth {

    public function login($driver)
    {
        switch($driver) {
            case 'google':
                return $this->auth_google();
                break;
            case 'linkedin':
                return $this->auth_linkedin();
                break;
            default:
                show_404();
                break;
        }
    }
    
    private function auth_google()
    {
        $provider = new \League\OAuth2\Client\Provider\Google([
            'clientId'     => '123295264922-9ceuf5bas36qm08uok78dceepoffoev9.apps.googleusercontent.com',
            'clientSecret' => 'rvPW00-ZPVa9UyTMG_A0hFS9',
            'redirectUri'  => site_url('oauth2/google/callback'),
            'hostedDomain' => App::ci()->config->item('base_url'),
        ]);
        
        if (!empty($_GET['error'])) {
        
            // Got an error, probably user denied access
            exit('Got error: ' . htmlspecialchars($_GET['error'], ENT_QUOTES, 'UTF-8'));
        
        } elseif (empty($_GET['code'])) {
        
            // If we don't have an authorization code then get one
            $authUrl = $provider->getAuthorizationUrl();
            $_SESSION['oauth2state'] = $provider->getState();
            header('Location: ' . $authUrl);
            exit;
        
        } elseif (empty($_GET['state']) || ($_GET['state'] !== $_SESSION['oauth2state'])) {
        
            // State is invalid, possible CSRF attack in progress
            unset($_SESSION['oauth2state']);
            exit('Invalid state');
        
        } else {
        
            // Try to get an access token (using the authorization code grant)
            $token = $provider->getAccessToken('authorization_code', [
                'code' => $_GET['code']
            ]);
        
            // Optional: Now you have a token you can look up a users profile data
            try {
        
                // We got an access token, let's now get the owner details
                $ownerDetails = $provider->getResourceOwner($token);
            
                return $ownerDetails;
                // Use these details to create a new profile
                // printf('Hello %s!', $ownerDetails->getFirstName());
        
            } catch (Exception $e) {
        
                // Failed to get user details
                exit('Something went wrong: ' . $e->getMessage());
        
            }
        
            // Use this to interact with an API on the users behalf
            echo $token->getToken();
        
            // Use this to get a new access token if the old one expires
            echo $token->getRefreshToken();
        
            // Number of seconds until the access token will expire, and need refreshing
            echo $token->getExpires();
        }
    }
    
    private function auth_linkedin()
    {
        $provider = new \League\OAuth2\Client\Provider\LinkedIn([
            'clientId'          => '77h7aigp6xzsji',
            'clientSecret'      => 'wK1rCuym8G7MkluW',
            'redirectUri'       => site_url('oauth2/linkedin/callback'),
        ]);
        
        if (!isset($_GET['code'])) {
        
            // If we don't have an authorization code then get one
            $authUrl = $provider->getAuthorizationUrl();
            $_SESSION['oauth2state'] = $provider->getState();
            header('Location: '.$authUrl);
            exit;
        
        // Check given state against previously stored one to mitigate CSRF attack
        } elseif (empty($_GET['state']) || ($_GET['state'] !== $_SESSION['oauth2state'])) {
        
            unset($_SESSION['oauth2state']);
            exit('Invalid state');
        
        } else {
        
            // Try to get an access token (using the authorization code grant)
            $token = $provider->getAccessToken('authorization_code', [
                'code' => $_GET['code']
            ]);
        
            // Optional: Now you have a token you can look up a users profile data
            try {
        
                // We got an access token, let's now get the user's details
                $user = $provider->getResourceOwner($token);
                
                return $user;
                // Use these details to create a new profile
                // printf('Hello %s!', $user->getFirstname());
        
            } catch (Exception $e) {
        
                // Failed to get user details
                exit('Oh dear...');
            }
        
            // Use this to interact with an API on the users behalf
            echo $token->getToken();
        }
    }
}